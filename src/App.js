import React, { useEffect } from 'react';
import { BookConsignment } from './components/BookConsignment';
import './App.css';

function App() {
  useEffect(() => {
    fetch('http://localhost:3001/posts').then((data) => {
      console.log('data aaya', data);
    })

  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <h1>Consignment Project</h1>
        <BookConsignment />
      </header>
    </div>
  );
}

export default App;
