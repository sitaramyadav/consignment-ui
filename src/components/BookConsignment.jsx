import React, { useState } from 'react';
import { TextInput } from './TextInput';


export const BookConsignment = () => {
    const [source, setSource] = useState(null);
    const [label, setLabel] = useState(null);
    const onChange = (event) => {
        const value = event.target.value;
        setSource(value);
    }
    return (
        <TextInput
            name="from"
            label="From"
            value={source}
            onChange={onChange}
        />)
}